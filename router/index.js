const express = require("express");
const router = express.Router();
const bodyparser = require("body-parser"); 

router.get("/formulario", (req, res)=>{
    const valores ={
        numBoleto:req.query.numBoleto,
        edad:req.query.edad,
        tipoViaje:req.query.tipoViaje, 
        precio:req.query.precio,
        
    }
    res.render('formulario.html', valores); 
})

router.post("/formulario", (req, res)=>{
    const valores ={
        numBoleto:req.body.numBoleto, 
        edad:req.body.edad,
        tipoViaje:req.body.tipoViaje, 
        precio:req.body.precio,
        
    }
    res.render('formulario.html', valores); 
})
function limpiarCampos() {
    document.getElementById("numBoleto").value = "";
    document.getElementById("destino").value = "";
    document.getElementById("cliente").value = "";
    document.getElementById("edad").value = "";
    document.getElementById("precio").value = "";
    document.getElementById("subtotal").value = "";
    document.getElementById("impuesto").value = "";
    document.getElementById("descuento").value = "";
    document.getElementById("total").value = "";

}


module.exports=router;